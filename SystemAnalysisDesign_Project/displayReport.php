<?php
include 'Admin.php';
$report = new Admin();

$date = $_GET['date'];

if ($date != NULL) {

    echo "<h3>Daily sales report of $date</h3>";
    echo "<h4>Summary of sold record</h4>";
    $summary = $report->getSalesRecordSummary($date);
    $report->displaySalesRecordSummary($summary);

    echo "<h4>Sold food record</h4>";
    echo "<h5>Overall food record</h5>";
    $summary = $report->getFoodOverallSummary($date);
    $report->displayProductOverallSummary($summary);

    echo "<h5>Counts of food</h5>";
    $summary = $report->getFoodOnlySummary($date);
    $report->displayProductOnlySummary($summary);

    echo "<h5>Counts of upsize for food</h5>";
    $summary = $report->getFoodUpsizeOnlySummary($date);
    $report->displayProductUpsizeOnlySummary($summary);

    echo "<h4>Sold beverage record</h4>";
    echo "<h5>Overall beverage record</h5>";
    $summary = $report->getBeverageOverallSummary($date);
    $report->displayProductOverallSummary($summary);

    echo "<h5>Counts of beverage</h5>";
    $summary = $report->getBeverageOnlySummary($date);
    $report->displayProductOnlySummary($summary);

    echo "<h5>Counts of upsize for beverage</h5>";
    $summary = $report->getBeverageUpsizeOnlySummary($date);
    $report->displayProductUpsizeOnlySummary($summary);
}else{
    echo "<h5>Please select a date</h5>";
}