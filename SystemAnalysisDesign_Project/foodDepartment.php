<?php

// Start Session
session_start();

// check user login
if ((empty($_SESSION['user_id'])) || ($_SESSION['user_id'] != 3)) {
    header("Location: index.php");
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet" href="css/bootstrap-grid.css">
<link rel="stylesheet" href="css/bootstrap-grid.min.css">
<link rel="stylesheet" href="css/bootstrap-reboot.css">
<link rel="stylesheet" href="css/bootstrap-reboot.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap.min.css">

<title>Food Department</title>
</head>
<body>

	<script type="text/javascript">
		function realSysTime(clock){
			var now = new Date();
			var year = now.getFullYear();
			var month = now.getMonth();
			var date = now.getDate();
			var day = now.getDay();
			var hour = now.getHours();
			var minu = now.getMinutes();
			var sec = now.getSeconds();
			month = month + 1;
			var arr_week = new Array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
			var week = arr_week[day];
			var time = date + "-" + month + "-" + year + "-" + week + " " + hour + ":" + minu + ":" + sec;
			clock.innerHTML = "Time: " + time;
		}
		window.onload = function(){
			window.setInterval("realSysTime(clock)", 1000);
		}

		function updateOrder() {
			
		    if (window.XMLHttpRequest) {
		            // code for IE7+, Firefox, Chrome, Opera, Safari
		            xmlhttp = new XMLHttpRequest();
		        } else {
		            // code for IE6, IE5
		            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		        }
		        xmlhttp.onreadystatechange = function() {
		            if (this.readyState == 4 && this.status == 200) {
		            	document.getElementById("order").innerHTML = this.responseText;
		            }
		        };
		        xmlhttp.open("GET","refreshOrderFood.php", true);
		        xmlhttp.send();        
		}

		window.setInterval("updateOrder()", 5000);
		
		function updateStatus(id) {
			
			if (confirm("Confirm this product is done?")) {
		    	if (window.XMLHttpRequest) {
		            // code for IE7+, Firefox, Chrome, Opera, Safari
		            xmlhttp = new XMLHttpRequest();
		        } else {
		            // code for IE6, IE5
		            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		        }
		        xmlhttp.onreadystatechange = function() {
		            if (this.readyState == 4 && this.status == 200) {
		            	
		            }
		        };
		        xmlhttp.open("GET","updateStatus.php?id="+id, true);
		        xmlhttp.send();  
		        updateOrder();
			}
		}
	</script>

	<div class="fluid-container"
		style="height: 100%; background-color: #1A1A1A; color: #545454; font-weight: bolder;">

		<div class="row" style="height: 15%">
			<div class="col-sm-9" align="center">
				<p style="font-size: 50px;">Food department of Farid Cafe</p>
			</div>
			<div class="col-sm-3">
				<div id="clock" style="font-size: 20px;"></div>
			</div>
		</div>

		<div id="order" style="height: 80%; overflow-y: scroll;">
			<?php

include 'Kitchen.php';
$app = new Kitchen();
$order = $app->getOrderFood();
$app->displayOrder($order);
?>
		</div>
		<div style="height: 5%">
			<p>
				Copyright (c) 2018 BIC21003 System Analysis Design <a
					href="logout.php">Logout</a>
			</p>
		</div>

	</div>
</body>

</html>