<?php
include 'Library.php';

class Customer extends Library {
    /*
     * Get templete for displaying temporary data.
     *
     * @param VOID
     * @return String
     */
    public function displayTempData()
    {
        $tempList = Customer::getTempData();
        echo "<table class=\"table table-borderless\" style=\"color: #545454; font-weight: bolder;\">
                <tr>
                    <td>No.</td>
                    <td>Product</td>
                    <td>Up size</td>
                    <td>Quantity</td>
                    <td>Notes</td>
                    <td>Sub-total</td>
                </tr>";
        
        if (is_array($tempList)) {
            if (Customer::isMultidimensional($tempList)) {
                foreach ($tempList as $key) {
                    echo "<tr>";
                    echo "<td>" . $key['sold_product_id'] . "</td>";
                    echo "<td>" . $key['product_name'] . "</td>";
                    echo "<td>" . $key['up_size'] . "</td>";
                    echo "<td>" . $key['quantity'] . "</td>";
                    echo "<td>" . $key['notes'] . "</td>";
                    echo "<td>" . $key['subtotal'] . "</td>";
                    echo "</tr>";
                }
            } else {
                echo "<tr>";
                echo "<td>" . $tempList['sold_product_id'] . "</td>";
                echo "<td>" . $tempList['product_name'] . "</td>";
                echo "<td>" . $tempList['up_size'] . "</td>";
                echo "<td>" . $tempList['quantity'] . "</td>";
                echo "<td>" . $tempList['notes'] . "</td>";
                echo "<td>" . $tempList['subtotal'] . "</td>";
                echo "</tr>";
            }
        }
        echo "</table>";
    }
    
    /*
     * Get the sum of subtital from temporary table
     *
     * @param VOID
     * @return float
     */
    public function getSumTempData()
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT SUM(subtotal) AS sum FROM systemanalysisdesign.input");
            $query->execute();
            if ($query->rowCount() > 0) {
                // return $query->fetchAll();
                $result = $query->fetch(PDO::FETCH_OBJ);
                return $result->sum;
            }
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
    
    /*
     * Get the data from temporary table
     *
     * @param VOID
     * @return Mixed data type array
     */
    public function getTempData()
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT * FROM systemanalysisdesign.input");
            $query->execute();
            if ($query->rowCount() > 0) {
                return $query->fetchAll();
            }
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
    
}