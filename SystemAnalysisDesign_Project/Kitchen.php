<?php
include 'Library.php';

class Kitchen extends Library {
    
    /*
     * Get the haven't done food order from sales table
     * @param VOID
     * @return Mixed data type array
     */
    public function getOrderBeverage()
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT s.id AS id, p.product_name AS product_name, s.up_size AS up_size, s.notes AS notes, s.quantity AS quantity FROM systemanalysisdesign.sales AS s INNER JOIN systemanalysisdesign.beverage AS p ON s.product_id = p.product_id WHERE s.product_type_id = 2 AND s.product_status = 0;");
            $query->execute();
            $result = $query->fetchAll();
            return $result;
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
    
    /*
     * Get the haven't done beverage order from sales table
     *
     * @param VOID
     * @return Mixed data type array
     */
    public function getOrderFood()
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT s.id AS id, p.product_name AS product_name, s.up_size AS up_size, s.notes AS notes, s.quantity AS quantity FROM systemanalysisdesign.sales AS s INNER JOIN systemanalysisdesign.food AS p ON s.product_id = p.product_id WHERE s.product_type_id = 1 AND s.product_status = 0;");
            $query->execute();
            $result = $query->fetchAll();
            return $result;
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
    
    /*
     * Generate HTML code to display order
     *
     * @param $order
     * @return String
     */
    public function displayOrder($order)
    {
        if (!empty($order)) {
            if (Library::isMultidimensional($order)) {
                foreach ($order as $key) {
                    echo "<button type=\"button\" onclick=\"updateStatus(" . $key['id'] . ")\" class=\"btn btn-primary\" style=\"width: 350px; height: 110px; margin-top: 5px; margin-left: 5px; \">
                        Product name: " . $key['product_name'] . "</br>" .
                        "Up size: " . $key['up_size'] . "</br>" .
                        "Notes: " . $key['notes'] . "</br>" .
                        "Quantity: " . $key['quantity'] .
                        "</button>";
                }
            } else {
                echo "<button type=\"button\" onclick=\"updateStatus(" . $order['id'] . ")\" class=\"btn btn-primary\" style=\"width: 350px; height: 110px; margin-top: 5px; margin-left: 5px; \">
                        Product name: " . $order['product_name'] . "</br>" .
                        "Up size: " . $order['up_size'] . "</br>" .
                        "Notes: " . $order['notes'] . "</br>" .
                        "Quantity: " . $order['quantity'] .
                        "</button>";
            }
        }else{
            echo "There is no order in this second...";
        }
        
    }
    
    /*
     * Update the sales table once the order is done.
     *
     * @param $id
     * @return VOID
     */
    public function updateStatus($id){
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("UPDATE systemanalysisdesign.sales SET product_status = 1 WHERE id = :id;");
            $query->bindParam(":id", $id, PDO::PARAM_STR);
            $query->execute();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
    
}