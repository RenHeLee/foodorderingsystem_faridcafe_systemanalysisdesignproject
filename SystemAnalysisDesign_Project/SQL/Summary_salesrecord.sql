SELECT SUM(total) AS subtotal,
SUM(discount) AS subdiscount,
SUM(serc) AS subserc,
SUM(grand * extradisc / 100) AS subextradisc,
SUM(roundingadj) AS subroundingadj,
SUM(tendered) AS subtendered
FROM systemanalysisdesign.salesrecord
WHERE transactiondate = '2018-11-26'
AND void IS NOT NULL
;