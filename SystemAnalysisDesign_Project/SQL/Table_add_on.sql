CREATE TABLE add_on(
add_on_id VARCHAR(5) NOT NULL PRIMARY KEY,
product_type_id INT(1) NOT NULL,
add_on_name VARCHAR(50) NOT NULL,
add_on_price NUMERIC(4,2) NOT NULL
)