CREATE TABLE systemanalysisdesign.input(
sold_product_id INT(10) NOT NULL PRIMARY KEY ,
product_id VARCHAR(5) NOT NULL,
up_size INT(1) NOT NULL,
quantity INT(3) NOT NULL,
notes VARCHAR(500),
subtotal NUMERIC(7,2) NOT NULL,
product_name VARCHAR(100) NOT NULL
)