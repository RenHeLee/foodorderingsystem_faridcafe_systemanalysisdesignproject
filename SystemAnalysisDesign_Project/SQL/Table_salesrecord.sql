CREATE TABLE  `salesrecord` (
`receiptid` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`cashierid` VARCHAR( 100 ) NOT NULL ,
`total` NUMERIC(7,2) NOT NULL ,
`discount` NUMERIC(7,2) NOT NULL ,
`grand` NUMERIC(7,2) NOT NULL ,
`extradisc` NUMERIC(7,2) NOT NULL ,
`serc` NUMERIC(7,2) NOT NULL ,
`befrounding` NUMERIC(7,2) NOT NULL,
`roundingadj` NUMERIC(7,2) NOT NULL,
`tendered` NUMERIC(7,2) NOT NULL,
`transactiondate` DATE  NOT NULL,
`transactiontime` TIME  NOT NULL,
`void` INT(1)
) ;