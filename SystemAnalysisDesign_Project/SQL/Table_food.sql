CREATE TABLE food(
product_id VARCHAR(5) NOT NULL PRIMARY KEY,
product_name VARCHAR(50) NOT NULL,
product_type_id INT(1) NOT NULL,
price NUMERIC(4,2) NOT NULL
)