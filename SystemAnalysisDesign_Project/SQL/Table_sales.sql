CREATE TABLE sales(
id INT(255) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
receiptid VARCHAR(7) NOT NULL,
product_id VARCHAR(5) NOT NULL,
up_size INT(2) NOT NULL,
product_type_id INT(1) NOT NULL,
notes VARCHAR(500),
quantity INT(3) NOT NULL,
product_status INT(1) NOT NULL
)