SELECT s.up_size AS upsize, SUM(s.quantity) AS quantity

FROM systemanalysisdesign.salesrecord AS sr
INNER JOIN systemanalysisdesign.sales AS s
ON sr.receiptid = s.receiptid
WHERE sr.transactiondate = '2018-11-26' 
AND s.product_type_id = 1
GROUP BY s.up_size;