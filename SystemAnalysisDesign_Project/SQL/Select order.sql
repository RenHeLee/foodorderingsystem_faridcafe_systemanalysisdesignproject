SELECT s.id AS id, s.receiptid AS receiptid,p.product_name AS product_name,s.up_size AS up_size,s.notes AS notes,s.quantity AS quantity
	FROM systemanalysisdesign.sales AS s INNER JOIN systemanalysisdesign.beverage AS p
		ON s.product_id = p.product_id
WHERE s.product_type_id = 2
	AND s.product_status = 0;