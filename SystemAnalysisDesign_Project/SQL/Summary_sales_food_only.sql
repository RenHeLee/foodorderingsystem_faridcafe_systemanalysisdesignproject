SELECT s.product_id AS id, p.product_name AS product, SUM(s.quantity) AS quantity
FROM systemanalysisdesign.salesrecord AS sr
INNER JOIN systemanalysisdesign.sales AS s
ON sr.receiptid = s.receiptid
INNER JOIN systemanalysisdesign.food AS p
ON s.product_id = p.product_id
WHERE sr.transactiondate = '2018-11-26' sr.void IS NOT NULL
GROUP BY s.product_id;