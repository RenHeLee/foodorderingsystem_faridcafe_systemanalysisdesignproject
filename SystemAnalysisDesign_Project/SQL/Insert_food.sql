START TRANSACTION;
INSERT INTO food VALUES ('F101', 'Nasi Goreng Biasa', '1', '4.00');
INSERT INTO food VALUES ('F102', 'Nasi Goreng Cina', '1', '4.00');
INSERT INTO food VALUES ('F103', 'Nasi Goreng Kampung', '1', '4.00');
INSERT INTO food VALUES ('F104', 'Nasi Goreng Ayam', '1', '6.00');
INSERT INTO food VALUES ('F105', 'Nasi Goreng Cili Padi', '1', '4.50');
INSERT INTO food VALUES ('F201', 'Nasi Ambeng', '1', '4.00');
INSERT INTO food VALUES ('F202', 'Nasi Lemak', '1', '1.50');
INSERT INTO food VALUES ('F301', 'Mi Goreng', '1', '4.00');
INSERT INTO food VALUES ('F302', 'Mi Sup', '1', '5.00');
INSERT INTO food VALUES ('F303', 'Mi Goreng Mamak', '1', '4.50');
INSERT INTO food VALUES ('F401', 'Bee Hoon Goreng', '1', '4.00');
INSERT INTO food VALUES ('F402', 'Bee Hoon Sup', '1', '5.00');
INSERT INTO food VALUES ('F403', 'Bee Hoon Goreng Mamak', '1', '4.50');
INSERT INTO food VALUES ('F404', 'Bee Hoon Goreng Kampung', '1', '4.50');
INSERT INTO food VALUES ('F501', 'Kuew Teow Goreng', '1', '4.00');
INSERT INTO food VALUES ('F502', 'Kuew Teow Sup', '1', '5.00');
INSERT INTO food VALUES ('F503', 'Kuew Teow Goreng Mamak', '1', '4.50');
INSERT INTO food VALUES ('F504', 'Kuew Teow Goreng Kampung', '1', '4.50');
INSERT INTO food VALUES ('F601', 'Maggi Goreng', '1', '4.00');
INSERT INTO food VALUES ('F602', 'Maggi Sup', '1', '4.50');
INSERT INTO food VALUES ('F603', 'Maggi Kari', '1', '4.50');
INSERT INTO food VALUES ('F701', 'Ayam Goreng', '1', '2.00');
INSERT INTO food VALUES ('F702', 'Telur', '1', '1.00');
COMMIT;