<?php

include 'database.php';
include 'Cashier.php';

// $app = new Library();
class Admin extends Cashier
{
    /*
     * getFirstDate
     * 
     * Get minimun date from database "systemanalysisdesign.salesrecord"
     * 
     * @param VOID
     * @return String
     * 
     */
    public function getFirstDate()
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT MIN(transactiondate) AS mindate FROM systemanalysisdesign.salesrecord;");
            $query->execute();
            $result = $query->fetch(PDO::FETCH_OBJ);
            return $result->mindate;
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
        ;
    }

    /*
     * getLastDate
     *
     * Get maximun date from database "systemanalysisdesign.salesrecord"
     *
     * @param VOID
     * @return String
     *
     */
    public function getLastDate()
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT MAX(transactiondate) AS maxdate FROM systemanalysisdesign.salesrecord;");
            $query->execute();
            $result = $query->fetch(PDO::FETCH_OBJ);
            return $result->maxdate;
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
        ;
    }

    /*
     * getSalesRecordSummary
     *
     * Get summary sales record
     *
     * @param $date
     * @return Mixed data types array
     *
     */
    public function getSalesRecordSummary($date)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT SUM(total) AS subtotal, SUM(discount) AS subdiscount, SUM(serc) AS subserc, SUM(grand * extradisc / 100) AS subextradisc, SUM(roundingadj) AS subroundingadj, SUM(tendered) AS subtendered FROM systemanalysisdesign.salesrecord WHERE transactiondate = :date AND void IS NULL;");
            $query->bindParam(":date", $date, PDO::PARAM_STR);
            $query->execute();
            $result = $query->fetchAll();
            return $result;
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
        ;
    }
    
    /*
     * getFoodOverallSummary
     *
     * Get summary daily food sold record (Overall)
     *
     * @param $date
     * @return Mixed data types array
     *
     */
    public function getFoodOverallSummary($date)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT s.product_id AS id, p.product_name AS product, s.up_size AS upsize, SUM(s.quantity) AS quantity FROM systemanalysisdesign.salesrecord AS sr INNER JOIN systemanalysisdesign.sales AS s ON sr.receiptid = s.receiptid INNER JOIN systemanalysisdesign.food AS p ON s.product_id = p.product_id WHERE sr.transactiondate = :date AND sr.void IS NULL GROUP BY s.product_id,s.up_size;");
            $query->bindParam(":date", $date, PDO::PARAM_STR);
            $query->execute();
            $result = $query->fetchAll();
            return $result;
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
        ;
    }

    /*
     * getFoodOnlySummary
     *
     * Get summary daily food sold record (by food type)
     *
     * @param $date
     * @return Mixed data types array
     *
     */
    public function getFoodOnlySummary($date)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT s.product_id AS id, p.product_name AS product, SUM(s.quantity) AS quantity FROM systemanalysisdesign.salesrecord AS sr INNER JOIN systemanalysisdesign.sales AS s ON sr.receiptid = s.receiptid INNER JOIN systemanalysisdesign.food AS p ON s.product_id = p.product_id WHERE sr.transactiondate = :date AND sr.void IS NULL GROUP BY s.product_id;");
            $query->bindParam(":date", $date, PDO::PARAM_STR);
            $query->execute();
            $result = $query->fetchAll();
            return $result;
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
        ;
    }

    /*
     * getFoodOnlySummary
     *
     * Get summary daily food sold record (By upsize)
     *
     * @param $date
     * @return Mixed data types array
     *
     */
    public function getFoodUpsizeOnlySummary($date)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT s.up_size AS upsize, SUM(s.quantity) AS quantity FROM systemanalysisdesign.salesrecord AS sr INNER JOIN systemanalysisdesign.sales AS s ON sr.receiptid = s.receiptid WHERE sr.transactiondate = :date AND sr.void IS NULL AND s.product_type_id = 1 AND sr.void IS NULL GROUP BY s.up_size;");
            $query->bindParam(":date", $date, PDO::PARAM_STR);
            $query->execute();
            $result = $query->fetchAll();
            return $result;
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
        ;
    }

    /*
     * getBeverageOnlySummary
     *
     * Get summary daily beverage sold record (Overall)
     *
     * @param $date
     * @return Mixed data types array
     *
     */
    public function getBeverageOverallSummary($date)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT s.product_id AS id, p.product_name AS product, s.up_size AS upsize, SUM(s.quantity) AS quantity FROM systemanalysisdesign.salesrecord AS sr INNER JOIN systemanalysisdesign.sales AS s ON sr.receiptid = s.receiptid INNER JOIN systemanalysisdesign.beverage AS p ON s.product_id = p.product_id WHERE sr.transactiondate = :date AND sr.void IS NULL GROUP BY s.product_id,s.up_size;");
            $query->bindParam(":date", $date, PDO::PARAM_STR);
            $query->execute();
            $result = $query->fetchAll();
            return $result;
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
        ;
    }

    /*
     * getBeverageOnlySummary
     *
     * Get summary daily beverage sold record (By beverage only)
     *
     * @param $date
     * @return Mixed data types array
     *
     */
    public function getBeverageOnlySummary($date)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT s.product_id AS id, p.product_name AS product, SUM(s.quantity) AS quantity FROM systemanalysisdesign.salesrecord AS sr INNER JOIN systemanalysisdesign.sales AS s ON sr.receiptid = s.receiptid INNER JOIN systemanalysisdesign.beverage AS p ON s.product_id = p.product_id WHERE sr.transactiondate = :date AND sr.void IS NULL GROUP BY s.product_id;");
            $query->bindParam(":date", $date, PDO::PARAM_STR);
            $query->execute();
            $result = $query->fetchAll();
            return $result;
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
        ;
    }

    /*
     * getBeverageOnlySummary
     *
     * Get summary daily beverage sold record (By upsize)
     *
     * @param $date
     * @return Mixed data types array
     *
     */
    public function getBeverageUpsizeOnlySummary($date)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT s.up_size AS upsize, SUM(s.quantity) AS quantity FROM systemanalysisdesign.salesrecord AS sr INNER JOIN systemanalysisdesign.sales AS s ON sr.receiptid = s.receiptid WHERE sr.transactiondate = :date AND sr.void IS NULL AND s.product_type_id = 2 GROUP BY s.up_size;");
            $query->bindParam(":date", $date, PDO::PARAM_STR);
            $query->execute();
            $result = $query->fetchAll();
            return $result;
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * getSalesRecord
     *
     * Get sold record (By receipt)
     *
     * @param $date
     * @return Mixed data types array
     *
     */
    public function getSalesRecord($date)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT receiptid as id, tendered, transactiontime FROM systemanalysisdesign.salesrecord WHERE transactiondate = :date AND void IS NULL");
            $query->bindParam(":date", $date, PDO::PARAM_STR);
            $query->execute();
            $result = $query->fetchAll();
            return $result;
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*function getSingleSalesRecord($date, $id)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT receiptid as id, tendered, transactiontime FROM systemanalysisdesign.salesrecord WHERE transactiondate = :date AND void IS NULL AND receiptid = :id");
            $query->bindParam(":date", $date, PDO::PARAM_STR);
            $query->bindParam(":id", $id, PDO::PARAM_STR);
            $query->execute();
            $result = $query->fetchAll();
            return $result;
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }*/
    
    /*
     * getSalesDetail
     *
     * Get detail of sold record (By receipt)
     *
     * @param $date
     * @return Mixed data types array
     *
     */
    public function getSalesDetail($id)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT p.product, s.up_size, s.notes, s.quantity, ((p.price + (s.up_size * 0.50)) * s.quantity) AS subtotal FROM systemanalysisdesign.sales AS s INNER JOIN (SELECT product_id AS id, product_name as product, price FROM systemanalysisdesign.food UNION SELECT product_id AS id, product_name AS product, price FROM systemanalysisdesign.beverage) AS p ON s.product_id = p.id WHERE s.receiptid = :id");
            $query->bindParam(":id", $id, PDO::PARAM_STR);
            $query->execute();
            $result = $query->fetchAll();
            return $result;
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * displaySalesRecordSummary
     *
     * Display summary daily sales record
     *
     * @param Mixed data types array
     * @return VOID
     *
     */
    public function displaySalesRecordSummary($summary)
    {
        if (! is_null($summary[0]['subtotal'])) {
            echo "<table class=\"table table-borderless\" style=\"color: #545454;\">
                <tr>
                    <td>Sub-total (RM): </td>
                    <td>" . $summary[0]['subtotal'] . "</td>
                </tr>
                <tr>
                    <td>Sub-discount (RM): </td>
                    <td>" . $summary[0]['subdiscount'] . "</td>
                </tr>
                <tr>
                    <td>Sub-service charges (RM): </td>
                    <td>" . $summary[0]['subserc'] . "</td>
                </tr>
                <tr>
                    <td>Sub-extra discount (RM): </td>
                    <td>" . round($summary[0]['subextradisc'], 2) . "</td>
                </tr>
                <tr>
                    <td>Sub-rounding adjustment (RM): </td>
                    <td>" . $summary[0]['subroundingadj'] . "</td>
                </tr>
                <tr>
                    <td>Sub-tendered (RM): </td>
                    <td>" . $summary[0]['subtendered'] . "</td>
                </tr>
            </table>";
        } else {
            echo "No record was recorded";
        }
    }

    /*
     * displayProductOverallSummary
     *
     * Display summary daily product sold record (Overall)
     *
     * @param Mixed data types array
     * @return VOID
     *
     */
    public function displayProductOverallSummary($summary)
    {
        if (! empty($summary)) {
            echo "<table class=\"table table-borderless\" style=\"color: #545454;\">
                <tr>
                    <td>Id</td>
                    <td>Product</td>
                    <td>Up size</td>
                    <td>Quantity</td>
                </tr>";

            if (is_array($summary)) {
                if (Admin::isMultidimensional($summary)) {
                    foreach ($summary as $key) {
                        echo "<tr>";
                        echo "<td>" . $key['id'] . "</td>";
                        echo "<td>" . $key['product'] . "</td>";
                        echo "<td>" . $key['upsize'] . "</td>";
                        echo "<td>" . $key['quantity'] . "</td>";
                        echo "</tr>";
                    }
                } else {
                    echo "<tr>";
                    echo "<td>" . $summary['id'] . "</td>";
                    echo "<td>" . $summary['product'] . "</td>";
                    echo "<td>" . $summary['upsize'] . "</td>";
                    echo "<td>" . $summary['quantity'] . "</td>";
                    echo "</tr>";
                }
            }
            echo "</table>";
        } else {
            echo "No record was recorded";
        }
    }

    /*
     * displayProductOnlySummary
     *
     * Display summary daily product sold record (by product type)
     *
     * @param Mixed data types array
     * @return VOID
     *
     */
    public function displayProductOnlySummary($summary)
    {
        if (! empty($summary)) {
            echo "<table class=\"table table-borderless\" style=\"color: #545454;\">
                <tr>
                    <td>Id</td>
                    <td>Product</td>
                    <td>Quantity</td>
                </tr>";

            if (is_array($summary)) {
                if (Admin::isMultidimensional($summary)) {
                    foreach ($summary as $key) {
                        echo "<tr>";
                        echo "<td>" . $key['id'] . "</td>";
                        echo "<td>" . $key['product'] . "</td>";
                        echo "<td>" . $key['quantity'] . "</td>";
                        echo "</tr>";
                    }
                } else {
                    echo "<tr>";
                    echo "<td>" . $summary['id'] . "</td>";
                    echo "<td>" . $summary['product'] . "</td>";
                    echo "<td>" . $summary['quantity'] . "</td>";
                    echo "</tr>";
                }
            }
            echo "</table>";
        } else {
            echo "No record was recorded";
        }
    }

    /*
     * displayProductUpsizeOnlySummary
     *
     * Display summary daily product sold record (By upsize)
     *
     * @param Mixed data types array
     * @return VOID
     *
     */
    public function displayProductUpsizeOnlySummary($summary)
    {
        if (! empty($summary)) {
            echo "<table class=\"table table-borderless\" style=\"color: #545454;\">
                <tr>
                    <td>Up size</td>
                    <td>Quantity</td>
                </tr>";

            if (is_array($summary)) {
                if (Admin::isMultidimensional($summary)) {
                    foreach ($summary as $key) {
                        echo "<tr>";
                        echo "<td>" . $key['upsize'] . "</td>";
                        echo "<td>" . $key['quantity'] . "</td>";
                        echo "</tr>";
                    }
                } else {
                    echo "<tr>";
                    echo "<td>" . $summary['upsize'] . "</td>";
                    echo "<td>" . $summary['quantity'] . "</td>";
                    echo "</tr>";
                }
            }
            echo "</table>";
        } else {
            echo "No record was recorded";
        }
    }

    /*
     * displaySalesRecord
     *
     * Display summary daily sales record
     *
     * @param Mixed data types array
     * @return VOID
     *
     */
    public function displaySalesRecord($record)
    {
        if (! empty($record)) {
            echo "<table class=\"table table-borderless\" style=\"color: #545454;\">
                <tr>
                    <td>ID</td>
                    <td>Tentered (RM)</td>
                    <td>Time</td>
                    <td>Void</td>
                </tr>";

            if (is_array($record)) {
                if (Admin::isMultidimensional($record)) {
                    foreach ($record as $key) {
                        echo "<tr>";
                        echo "<td>" . $key['id'] . "</td>";
                        echo "<td>" . $key['tendered'] . "</td>";
                        echo "<td>" . $key['transactiontime'] . "</td>";
                        echo "<td><button type=\"button\" class=\"btn btn-primary\" onclick=\"displayRecord(" . $key['id'] . ")\">Select</td>";
                        echo "</tr>";
                    }
                } else {
                    echo "<tr>";
                    echo "<td><a onclick=\"displayRecord(" . $record['id'] . ")\">" . $record['id'] . "</td>";
                    echo "<td>" . $record['tendered'] . "</td>";
                    echo "<td>" . $record['transactiontime'] . "</td>";
                    echo "<td><button type=\"button\" class=\"btn btn-primary\" onclick=\"displayRecord(" . $record['id'] . ")\">Select</td>";
                    echo "</tr>";
                }
            }
            echo "</table>";
        } else {
            echo "No record was recorded";
        }
    }

    /*
     * displaySalesDetail
     *
     * Display detail of sold record (By receipt)
     *
     * @param Mixed data types array
     * @return VOID
     *
     */
    public function displaySalesDetail($detail)
    {
        echo "<table class=\"table table-borderless\" style=\"color: #545454;\">
                <tr>
                    <td>Product: </td>
                    <td>Up size</td>
                    <td>Notes</td>
                    <td>Quantity</td>
                    <td>Subtotal (RM)</td>
                </tr>";

        if (is_array($detail)) {
            if (Admin::isMultidimensional($detail)) {
                foreach ($detail as $key) {
                    echo "<tr>";
                    echo "<td>" . $key['product'] . "</td>";
                    echo "<td>" . $key['up_size'] . "</td>";
                    echo "<td>" . $key['notes'] . "</td>";
                    echo "<td>" . $key['quantity'] . "</td>";
                    echo "<td>" . $key['subtotal'] . "</td>";
                    echo "</tr>";
                }
            } else {
                echo "<tr>";
                echo "<td>" . $detail['product'] . "</td>";
                echo "<td>" . $detail['up_size'] . "</td>";
                echo "<td>" . $detail['notes'] . "</td>";
                echo "<td>" . $detail['quantity'] . "</td>";
                echo "<td>" . $detail['subtotal'] . "</td>";
                echo "</tr>";
            }
        }
        echo "</table>";
    }

    /*
     * updateVoid
     *
     * Update the salesrecord to void the record of the $id
     *
     * @param $id
     * @return VOID
     *
     */
    public function updateVoid($id)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("UPDATE systemanalysisdesign.salesrecord SET void=1 WHERE receiptid=:id;");
            $query->bindParam(":id", $id, PDO::PARAM_STR);
            $query->execute();
            $result = $query->fetchAll();
            return $result;
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
        ;
    }

    /*
     * insertBillSalesTable
     *
     * Display summary daily sales record
     *
     * @param $receiptid
     * @return VOID
     *
     */
    public function insertBillSalesTable($receiptid)
    {
        $tempList = Admin::getTempData();

        try {
            global $pdo;
            $db = $pdo;
            $db->beginTransaction();
            if (Admin::isMultidimensional($tempList)) {
                foreach ($tempList as $key) {
                    $product_id = $key["product_id"];
                    $up_size = $key['up_size'];
                    $notes = $key['notes'];
                    $quantity = $key['quantity'];
                    $product_status = 1;

                    $firstletter_id = substr($product_id, 0, 1);
                    if ($firstletter_id == "F") {
                        $product_type_id = 1;
                    } else {
                        $product_type_id = 2;
                    }

                    $query = $db->prepare("INSERT INTO systemanalysisdesign.sales
(receiptid, product_id, up_size, product_type_id, notes, quantity, product_status)
VALUES (:receiptid, :product_id, :up_size, :product_type_id, :notes, :quantity, :product_status);");
                    $query->bindParam(":receiptid", $receiptid, PDO::PARAM_STR);
                    $query->bindParam(":product_id", $product_id, PDO::PARAM_STR);
                    $query->bindParam(":up_size", $up_size, PDO::PARAM_STR);
                    $query->bindParam(":product_type_id", $product_type_id, PDO::PARAM_STR);
                    $query->bindParam(":notes", $notes, PDO::PARAM_STR);
                    $query->bindParam(":quantity", $quantity, PDO::PARAM_STR);
                    $query->bindParam(":product_status", $product_status, PDO::PARAM_STR);

                    $query->execute();
                }
            } else {
                $product_id = $tempList["product_id"];
                $up_size = $tempList['up_size'];
                $notes = $tempList['notes'];
                $quantity = $tempList['quantity'];
                $product_status = 1;

                $firstletter_id = substr($product_id, 0, 1);
                if ($firstletter_id == "F") {
                    $product_type_id = 1;
                } else {
                    $product_type_id = 2;
                }

                $query = $db->prepare("INSERT INTO systemanalysisdesign.sales
(receiptid, product_id, up_size, product_type_id, notes, quantity, product_status)
VALUES (:receiptid, :product_id, :up_size, :product_type_id, :notes, :quantity, :product_status);");
                $query->bindParam(":receiptid", $receiptid, PDO::PARAM_STR);
                $query->bindParam(":product_id", $product_id, PDO::PARAM_STR);
                $query->bindParam(":up_size", $up_size, PDO::PARAM_STR);
                $query->bindParam(":product_type_id", $product_type_id, PDO::PARAM_STR);
                $query->bindParam(":notes", $notes, PDO::PARAM_STR);
                $query->bindParam(":quantity", $quantity, PDO::PARAM_STR);
                $query->bindParam(":product_status", $product_status, PDO::PARAM_STR);

                $query->execute();
            }
            $db->commit();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
        Admin::dropTempTable();
        Admin::createTempTable();
    }
}