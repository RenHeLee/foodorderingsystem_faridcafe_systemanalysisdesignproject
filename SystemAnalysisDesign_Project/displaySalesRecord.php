<?php
include 'Admin.php';
$app = new Admin();

date_default_timezone_set("Asia/Kuala_Lumpur");
$date = date("Y-m-d");

$record = $app->getSalesRecord($date);

echo "<div style=\"height: 8%;\" >";
echo "<h5>Sales record of $date</h5>";
echo "</div>";

echo "<div style=\"overflow-y: scroll; height: 92%;\" >";
$app->displaySalesRecord($record);
echo "</div>";