<?php

// Start Session
session_start();

// Application library ( with DemoLib class )
require 'Login.php';
$app = new Login();

$login_error_message = '';

// check Login request
if (! empty($_POST['signInForm'])) {

    $userInput = trim($_POST['userInput']);
    $password = trim($_POST['password']);

    $userDetail = $app->loginRequest($userInput, $password); // check user login

    print_r($userDetail);

    if (! is_null($userDetail)) {
        // Set Session
        $_SESSION['user_id'] = $userDetail[0]['user_id'];
        $_SESSION['username'] = $userDetail[0]['username'];
        $_SESSION['user_post'] = $userDetail[0]['user_post'];
        switch ($userDetail[0]['user_post']) {
            case 1:
                header("Location: cashierSite.php"); // Redirect user to the profile.php;
                break;
            case 2:
                header("Location: customerOrder.php"); // Redirect user to the profile.php;
                break;
            case 3:
                header("Location: foodDepartment.php"); // Redirect user to the profile.php;
                break;
            case 4:
                header("Location: beverageDepartment.php"); // Redirect user to the profile.php;
                break;
            case 5:
                header("Location: adminSite.php"); // Redirect user to the profile.php;
                break;
            default:
                ;
                break;
        }

        // header("Location: cashier_demo.php"); // Redirect user to the profile.php
    } else {
        $login_error_message = 'Invalid login details!';
    }
}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link rel="stylesheet" href="css/bootstrap-grid.css">
<link rel="stylesheet" href="css/bootstrap-grid.min.css">
<link rel="stylesheet" href="css/bootstrap-reboot.css">
<link rel="stylesheet" href="css/bootstrap-reboot.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap.min.css">

<title>Point of Sales System for Farid Cafe</title>
</head>
<body>
	<div class="fluid-container"
		style="height: 100%; background-color: #1A1A1A; color: #545454;">

		<div style="height: 15%">
			<h1 style="text-align: center; font-weight: bolder;">Point of Sales
				System for Farid Cafe</h1>
		</div>
		<div style="height: 85%;">
			<div class="card bg-primary text-white" style="width: 300px; margin-left:auto; margin-right: auto; margin-top: auto; margin-bottom: auto;">
				<div class="card-body">
					<form action=index.php method="post">
						<legend>Sign in</legend>
		<?php
if ($login_error_message != "") {
    echo '<div class="alert alert-danger"><strong>Error: </strong> ' . $login_error_message . '</div>';
}
?>
		<table>
							<tr>
								<td><label for="userInput">Username: </label></td>
								<td><input id="userInput" name="userInput" type="text" required></td>
							</tr>
							<tr>
								<td><label for="password">Password: </label></td>
								<td><input id="password" name="password" type="password"
									required></td>
							</tr>
						</table>
						<input type="submit" value="Submit" name="signInForm"
							class="btn btn-info" /> <input type="reset" value="Reset"
							class="btn btn-info" />
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>