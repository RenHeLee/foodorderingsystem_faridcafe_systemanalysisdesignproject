<?php

// Start Session
session_start();

// check user login
if ((empty($_SESSION['user_id'])) || ($_SESSION['user_post'] != 5)) {
    header("Location: index.php");
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/bootstrap-grid.css">
<link rel="stylesheet" href="css/bootstrap-grid.min.css">
<link rel="stylesheet" href="css/bootstrap-reboot.css">
<link rel="stylesheet" href="css/bootstrap-reboot.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap.min.css">


<title>Insert bill</title>
<style>
#index {
	display: block;
}

#food {
	display: none;
}

#beverage {
	display: none;
}

#order {
	display: none;
}

#deleteItem {
	display: none;
}

#payment {
	display: none;
}

#afterPayment {
	display: none;
}

font {
	color: #545454;
	font-weight: bolder;
}

ordertable {
	color: #545454;
	font-weight: bolder;
}
</style>

<?php
include 'Admin.php';

$app = new Admin();

$firstDate = $app->getFirstDate();
$lastDate = $app->getLastDate();

?>
</head>
<body>
	<script type="text/javascript">
	var counter = 1;

	
function clickType(type) {
    var index = document.getElementById("index");
    var food = document.getElementById("food");
    var beverage = document.getElementById("beverage");
    var order = document.getElementById("order");
    var deleteItem = document.getElementById("deleteItem");
    var payment = document.getElementById("payment");
    var afterPayment = document.getElementById("afterPayment");
	var listOftotalPrice = document.getElementById("listOftotalPrice");
    
	if (type == 1) {
		index.style.display = "none";
	    food.style.display = "block";
	    beverage.style.display = "none";
	    order.style.display = "none";
	    deleteItem.style.display = "none";
	    payment.style.display = "none";
	    afterPayment.style.display = "none";
	} else if (type == 2) {
		index.style.display = "none";
	    food.style.display = "none";
	    beverage.style.display = "block";
	    order.style.display = "none";
	    deleteItem.style.display = "none";
	    payment.style.display = "none";
	    afterPayment.style.display = "none";
	} else if (type == 3) {
		index.style.display = "none";
	    food.style.display = "none";
	    beverage.style.display = "none";
	    order.style.display = "none";
	    deleteItem.style.display = "block";
	    payment.style.display = "none";
	    afterPayment.style.display = "none";
	} else if (type == 4) {
		index.style.display = "none";
	    food.style.display = "none";
	    beverage.style.display = "none";
	    order.style.display = "none";
	    deleteItem.style.display = "none";
	    payment.style.display = "block";
	    afterPayment.style.display = "none";
	    listOftotalPrice.style.display = "none";
	    tempTotal();
	}
}

function clickProduct(product_id, product_name, price) {
    var index = document.getElementById("index");
    var food = document.getElementById("food");
    var beverage = document.getElementById("beverage");
    var order = document.getElementById("order");
    var deleteItem = document.getElementById("deleteItem");
    var payment = document.getElementById("payment");
    var afterPayment = document.getElementById("afterPayment");
    
    index.style.display = "none";
    food.style.display = "none";
    beverage.style.display = "none";
    order.style.display = "block";
    deleteItem.style.display = "none";
    payment.style.display = "none";
    afterPayment.style.display = "none";
    
    document.getElementById("pro_id").innerHTML = product_id;
    document.getElementById("product_name").innerHTML = product_name;
    document.getElementById("price").innerHTML = price;
    document.getElementById("subtotalSingleItem").innerHTML = price;
}

function tempTotal(){
    
    
    var total = parseFloat(document.getElementById("listOftotalPrice_total").innerHTML);
    var discount = parseFloat(document.getElementById("payment_discount").value);
    var grand;
    var extraDiscount = parseFloat(document.getElementById("payment_extraDiscount").value);
    var serviceTax = 0;
    var servc;
    var befRounding;
    var befRoundingAdj;
    var roundingAdj;
    var tendered;
    
    grand = total - discount;
    servc = grand * (serviceTax/100);
    befRounding = grand * ((100 - extraDiscount) / 100) * ((100 - serviceTax)/100);
    befRoundingAdj = parseInt(befRounding * 100) % 5;

    if(befRoundingAdj > 2){
    	roundingAdj = 5 - befRoundingAdj;
    }else if (befRoundingAdj > 0){
    	roundingAdj = 0 - befRoundingAdj;
    }else{
    	roundingAdj = 0;
        }

    roundingAdj = parseFloat(roundingAdj)/100;
    tendered = befRounding + roundingAdj;
    
    document.getElementById("payment_total").innerHTML = total.toFixed(2);
    document.getElementById("payment_grand").innerHTML = grand.toFixed(2);
    document.getElementById("payment_serc").innerHTML = servc.toFixed(2);
    document.getElementById("payment_befRounding").innerHTML = befRounding.toFixed(2);
    document.getElementById("payment_roundingAdj").innerHTML = roundingAdj.toFixed(2);
    document.getElementById("payment_tendered").innerHTML = tendered.toFixed(2);
}

function subtotalSingleItem(){
	var price = parseFloat(document.getElementById("price").innerHTML);
    var upSize = parseInt(document.getElementById("up_size").value);
    var quantity = parseInt(document.getElementById("quantity").value);
    var subtotalSingleItem = (price + (upSize * 0.50)) * quantity;
    document.getElementById("subtotalSingleItem").innerHTML = subtotalSingleItem.toFixed(2);
    
}

function payment(){

	if (confirm("Confirm to proceed payment?")) {
	var cashierid = document.getElementById("cashier").innerHTML;
	var total = parseFloat(document.getElementById("listOftotalPrice_total").innerHTML);
    var discount = parseFloat(document.getElementById("payment_discount").value);
    var grand = parseFloat(document.getElementById("payment_grand").innerHTML);
    var extraDiscount = parseFloat(document.getElementById("payment_extraDiscount").value);
    var serviceTax = 0;
    var servc = parseFloat(document.getElementById("payment_serc").innerHTML);
    var befRounding = parseFloat(document.getElementById("payment_befRounding").innerHTML);
    var roundingAdj = parseFloat(document.getElementById("payment_roundingAdj").innerHTML);
    var tendered = parseFloat(document.getElementById("payment_tendered").innerHTML);
    var date = document.getElementById("date").value;
	var amount = parseFloat(document.getElementById("payment_amount").value);
	var balance;

    var payment = document.getElementById("payment");
    var afterPayment = document.getElementById("afterPayment");
    var listOfChosenItems = document.getElementById("listOfChosenItems");
    var listOftotalPrice = document.getElementById("listOftotalPrice");
    
	balance = amount - tendered;
	
	if(balance >= 0){
	    payment.style.display = "none";
	    afterPayment.style.display = "block";
	    listOfChosenItems.style.display = "none";
	    listOftotalPrice.style.display = "none";
	    transaction(cashierid, total, discount, grand, extraDiscount, servc, befRounding, roundingAdj, tendered, date);
	    document.getElementById("afterPayment_balance").innerHTML = balance.toFixed(2);
		setTimeout("refresh()", 0500);
		
	}else{
		document.getElementById("payment_message").innerHTML = "The amount is not enough";
	}
	}
}

function refresh(){
	updateListOfChosenItem();
	counter = 1;
	resetPayment();
}

function tempInsert(){
    var index = document.getElementById("index");
    var food = document.getElementById("food");
    var beverage = document.getElementById("beverage");
    var order = document.getElementById("order");
    var deleteItem = document.getElementById("deleteItem");
    var payment = document.getElementById("payment");
    var afterPayment = document.getElementById("afterPayment");
    
    var product_name = document.getElementById("product_name").innerHTML;
	var product_id = document.getElementById("pro_id").innerHTML;
	var price = parseFloat(document.getElementById("price").innerHTML);
    var upSize = parseInt(document.getElementById("up_size").value);
    var quantity = parseInt(document.getElementById("quantity").value);
    var notes = document.getElementById("notes").value;
    var subtotalSingleItem = (price + (upSize * 0.50)) * quantity;
    
    insertTempDatabase(counter, product_id, upSize, quantity, notes, subtotalSingleItem, product_name);
    
    
    index.style.display = "block";
    food.style.display = "none";
    beverage.style.display = "none";
    order.style.display = "none";
    deleteItem.style.display = "none";
    payment.style.display = "none";
    afterPayment.style.display = "none";

    document.getElementById("up_size").value = "0";
    document.getElementById("quantity").value = "1";
    document.getElementById("notes").value = "";

    counter++;
    tempTotal();
    tempPaymentAmount();

}

function tempDelete(){
	if (confirm("Confirm to delete selected product?")) {
    var index = document.getElementById("index");
    var food = document.getElementById("food");
    var beverage = document.getElementById("beverage");
    var order = document.getElementById("order");
    var deleteItem = document.getElementById("deleteItem");
    var payment = document.getElementById("payment");
    var afterPayment = document.getElementById("afterPayment");

    var itemNumber = document.getElementById("deleteItem_no").value;

    deleteTempDatabase(itemNumber);
    
    index.style.display = "block";
    food.style.display = "none";
    beverage.style.display = "none";
    order.style.display = "none";
    deleteItem.style.display = "none";
    payment.style.display = "none";
    afterPayment.style.display = "none";
    
    document.getElementById("deleteItem_no").value = "";
    tempPaymentAmount();
	}
}

function backIndex(){
	var index = document.getElementById("index");
    var food = document.getElementById("food");
    var beverage = document.getElementById("beverage");
    var order = document.getElementById("order");
    var deleteItem = document.getElementById("deleteItem");
    var payment = document.getElementById("payment");
    var afterPayment = document.getElementById("afterPayment");
    var listOftotalPrice = document.getElementById("listOftotalPrice");
    var listOfChosenItems = document.getElementById("listOfChosenItems");

    index.style.display = "block";
    food.style.display = "none";
    beverage.style.display = "none";
    order.style.display = "none";
    deleteItem.style.display = "none";
    payment.style.display = "none";
    afterPayment.style.display = "none";
    listOftotalPrice.style.display = "block";
    listOfChosenItems.style.display = "block";
}

function updateListOfChosenItem() {
	
    if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
            	document.getElementById("listOfChosenItems").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","refreshCustomerOrder.php", true);
        xmlhttp.send();        
}


function resetInsert(){
	backIndex();
    
    document.getElementById("up_size").value = "0";
    document.getElementById("quantity").value = "1";
    document.getElementById("notes").value = "";
}

function resetDelete(){
	backIndex();
    
    document.getElementById("deleteItem_no").value = "";
}

function resetPayment(){
	backIndex();
	
    document.getElementById("payment_discount").value = 0.00;
    document.getElementById("payment_extraDiscount").value = 0.00;
    document.getElementById("payment_amount").value = null;
}

function resetDiscount(){
	document.getElementById("payment_discount").value = 0.00;
	tempTotal();
}

function resetExtraDiscount(){
	document.getElementById("payment_extraDiscount").value = 0.00;
	tempTotal();
}

function resetAmount(){
	document.getElementById("payment_amount").value = null;
}

function insertTempDatabase(index, product_id, upSize, quantity, notes, subtotalSingleItem, product_name) {

    if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("listOfChosenItems").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","insertTemporaryTable.php?index="+index+"&id="+product_id+"&upsize="+upSize+"&notes="+notes+"&quantity="+quantity+"&subtotal="+subtotalSingleItem+"&product="+product_name, true);
        xmlhttp.send();
}


function deleteTempDatabase(index) {

    if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("listOfChosenItems").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","deleteTemporaryTable.php?index="+index, true);
        xmlhttp.send();
}

function tempPaymentAmount() {
	
    if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
            	document.getElementById("listOftotalPrice_total").innerHTML = this.responseText;
            	document.getElementById("payment_total").innerHTML = this.responseText;
            }
        };
        xmlhttp.open("GET","displayTotalAmount.php", true);
        xmlhttp.send();        
}

function transaction(cashierid, total, discount, grand, extraDiscount, servc, befRounding, roundingAdj, tendered, date) {

    if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                
            }
        };
        xmlhttp.open("GET","transactionAdmin.php?cashierid="+cashierid+"&total="+total+"&discount="+discount+"&grand="+grand+"&extradisc="+extraDiscount+"&serc="+servc+"&befrounding="+befRounding+"&roundingadj="+roundingAdj+"&tendered="+tendered+"&date="+date, true);
        xmlhttp.send();
}

function realSysTime(clock){
	var now = new Date();
	var year = now.getFullYear();
	var month = now.getMonth();
	var date = now.getDate();
	var day = now.getDay();
	var hour = now.getHours();
	var minu = now.getMinutes();
	var sec = now.getSeconds();
	month = month + 1;
	var arr_week = new Array("Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat");
	var week = arr_week[day];
	var time = date + "-" + month + "-" + year + "-" + week + " " + hour + ":" + minu + ":" + sec;
	clock.innerHTML = "Time: " + time ;
}
window.onload = function(){
	updateListOfChosenItem();
	window.setInterval("realSysTime(clock)", 1000);}


</script>

	<div class="fluid-container"
		style="height: 100%; background-color: #1A1A1A; color: #545454;">

				<div style="height: 10%">

			<nav class="navbar navbar-expand-sm bg-primary navbar-dark">
			<ul class="navbar-nav" style="color: #1A1A1A; font-weight: bolder;">
				<li class="nav-item"><h5 style="text-align: center; font-weight: bolder;">Insert bill</h5></li>
				<li class="nav-item"><a onclick="clickType(1)" style="margin-left: 50px; ">Food</a></li>
				<li class="nav-item"><a onclick="clickType(2)" style="margin-left: 50px; ">Beverage</a></li>
				<li class="nav-item"><a onclick="clickType(3)" style="margin-left: 50px; ">Delete</a></li>
				<li class="nav-item"><a onclick="clickType(4)" style="margin-left: 50px; ">Payment</a></li>
				<li class="nav-item"><p id="clock"
						style="margin-left: 600px; font-weight: bolder;"></p></li>
			</ul>
			</nav>
			<table
				style="margin-left: 50px; color: #545454; font-weight: bolder;">
				<tr>
					<td>Cashier:</td>
					<td id="cashier">Farad</td>
				</tr>
			</table>
		</div>
		<div style="height: 85%">
			<div class="row">
				<div class="col-sm-8" style="height: 100%">
					<div id="index"
						style="margin-left: auto; margin-right: auto; margin-top: auto; margin-bottom: auto; font-weight: bolder;">
    <?php
    $btns = $app->getProductType();
    echo $app->getTypesButton($btns);
    ?>
    <button onclick="clickType(3)" value="3"
							class="btn btn-outline-primary"
							style="width: 300px; margin-left: 1%; margin-bottom: 1%">Delete
							item</button>
						<button onclick="clickType(4)" value="4"
							class="btn btn-outline-primary"
							style="width: 300px; margin-left: 1%; margin-bottom: 1%">Payment</button>
					</div>
					<div id="food" style="height: 100%;">
						<div style="height: 5%;">
						<h5 style="margin-left: 50px">Food list</h5>
						</div>
						<div style="height: 85%; overflow-y: scroll;">
    <?php
    $btns = $app->getFood();
    echo $app->getItemsButton($btns);
    ?>
    </div>
						<div style="height: 10%">
							<button onclick="backIndex()" class="btn btn-outline-primary"
								style="width: 300px; margin-left: 70%">Back</button>
						</div>
					</div>
					<div id="beverage" style="height: 100% overflow-y: scroll;">
						<div style="height: 5%;">
						<h5 style="margin-left: 50px">Beverage list</h5>
						</div>
						<div style="height: 85%; overflow-y: scroll;">
    <?php
    $btns = $app->getBeverage();
    echo $app->getItemsButton($btns);
    ?> 
    </div>
						<div style="height: 10%">
							<button onclick="backIndex()" class="btn btn-outline-primary"
								style="width: 300px; margin-left: 70%">Back</button>
						</div>
					</div>
					<div id="order">
					<div style="height: 5%;">
						<h5 style="margin-left: 50px">Detail of product</h5>
						</div>
						<div style="height: 85%; overflow-y: scroll; overflow-x: hidden;">
							<table class="table table-borderless"
								style="margin-left: 50px; color: #545454; font-weight: bolder;">
								<tr>
									<td>Product id:</td>
									<td id="pro_id" class="font"></td>
								</tr>
								<tr>
									<td>Product name:</td>
									<td id="product_name"></td>
								</tr>
								<tr>
									<td>Price:</td>
									<td id="price"></td>
								</tr>
								<tr>
									<td>Up size:</td>
									<td><input type="number" id="up_size" name="up_size" value="0"
										oninput="subtotalSingleItem()"></td>
								</tr>
								<tr>
									<td>Quantity:</td>
									<td><input type="number" id="quantity" name="quantity"
										value="1" oninput="subtotalSingleItem()"></td>
								</tr>
								<tr>
									<td>Notes:</td>
									<td><input type="text" id="notes" name="notes" value=""></td>
								</tr>
								<tr>
									<td>Sub-total:</td>
									<td id="subtotalSingleItem"></td>
								</tr>
							</table>
						</div>
						<div style="height: 90%;">
							<button onclick="tempInsert()" class="btn btn-outline-primary"
								style="width: 300px; margin-left: 50px;">Add</button>
							<button onclick="resetInsert()" class="btn btn-outline-primary"
								style="width: 300px">Back</button>
						</div>
					</div>
					<div id="deleteItem"
						style="margin-left: 50px; color: #545454; font-weight: bolder;">
						<div style="height: 5%;">
						<h5v >Delete selected product</h5>
						</div>
						<div style="height: 85%">
							<p class="font">Enter the no. of product:</p>
							<input type="number" id="deleteItem_no" value=""
								style="width: 300px;"> <br>
							<button onclick="tempDelete()" class="btn btn-outline-primary"
								style="width: 300px; margin-top: 20px;">Delete</button>
						</div>

						<div style="height: 10%">
							<button onclick="resetDelete()" class="btn btn-outline-primary"
								style="width: 300px; margin-left: 70%">Back</button>
						</div>


					</div>
					<div id="payment">
						<div style="height: 5%;">
						<h5 style="margin-left: 50px">Payment</h5>
						</div>
						<div style="height: 85%;">
							<table class="table table-borderless"
								style="margin-left: 50px; color: #545454; font-weight: bolder;">
								<tr>
									<td>Total(RM):</td>
									<td id="payment_total"></td>
								</tr>
								<tr>
									<td>Discount(RM):</td>
									<td><input type="text" id="payment_discount" value="0.00"
										onchange="tempTotal()"></td>
									<td><button onclick="resetDiscount()"
											class="btn btn-outline-primary" style="width: 300px">Reset</button></td>
								</tr>
								<tr>
									<td>Grand:</td>
									<td id="payment_grand"></td>
								</tr>
								<tr>
									<td>Extra discount (0.00%):</td>
									<td><input type="text" id="payment_extraDiscount" value="0.00"
										onchange="tempTotal()"></td>
									<td><button onclick="resetExtraDiscount()"
											class="btn btn-outline-primary" style="width: 300px">Reset</button></td>
								</tr>
								<tr>
									<td>Serc.(0.00%):</td>
									<td id="payment_serc"></td>
								</tr>
								<tr>
									<td>Bef. Rounding(RM):</td>
									<td id="payment_befRounding"></td>
								</tr>
								<tr>
									<td>Rounding adj(RM):</td>
									<td id="payment_roundingAdj"></td>
								</tr>
								<tr>
									<td>Tendered(RM):</td>
									<td id="payment_tendered"></td>
								</tr>
								<tr>
									<td>Date:</td>
									<td><?php echo "<input type=\"date\" id=\"date\" value=\"null\" min=\"" . $firstDate . "\" max=\"" . $lastDate . "\">"; ?></td>
								</tr>
								<tr>
									<td>Enter amount of payment (RM):</td>
									<td><input type="text" id="payment_amount" value=""></td>
									<td><button onclick="resetAmount()"
											class="btn btn-outline-primary" style="width: 300px">Reset</button></td>
								</tr>
								<tr>
									<td id="payment_message"></td>
								</tr>
							</table>
							<button onclick="payment()" class="btn btn-outline-primary"
								style="width: 300px;">Payment</button>
						</div>
						<div style="height: 10%">
							<button onclick="resetPayment()" class="btn btn-outline-primary"
								style="width: 300px; margin-left: 70%">Back</button>
						</div>
					</div>
					<div id="afterPayment">
						<table class="table table-borderless"
							style="color: #545454; font-weight: bolder; font-size: 50px">
							<tr>
								<td>Balance(RM):</td>
								<td id="afterPayment_balance"></td>
							</tr>
						</table>
					</div>
				</div>
				<div class="col-sm-4" style="height: 100%">
					<div id="listOfChosenItems"
						style="overflow-y: visible; height: 90%"></div>
					<div id="listOftotalPrice" style="height: 10%">
						<table class="table table-borderless"
							style="font-size: 30px; color: #545454; font-weight: bolder;">
							<tr>
								<td class="font">Total(RM):</td>
								<td id="listOftotalPrice_total" class="font"></td>
							</tr>
						</table>
					</div>
				</div>
			</div>
		</div>



		<div style="height: 7%">
			<p>
				Copyright (c) 2018 BIC21003 System Analysis Design <a
					href="logout.php">Logout</a>
			</p>
		</div>

	</div>


</body>
</html>