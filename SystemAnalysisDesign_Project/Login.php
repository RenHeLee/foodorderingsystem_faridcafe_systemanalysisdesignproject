<?php
include 'database.php';

class Login {
    /*
     * Login
     *
     * @param $userInput, $password
     * @return $mixed
     */
    public function loginRequest($userInput, $password)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT * FROM systemanalysisdesign.user WHERE username =:userInput AND hashedpassword=:hashedPassword");
            $query->bindParam(":userInput", $userInput, PDO::PARAM_STR);
            $hashedPassword = hash('sha256', $password);
            $query->bindParam(":hashedPassword", $hashedPassword, PDO::PARAM_STR);
            $query->execute();
            if ($query->rowCount() > 0) {
                $result = $query->fetchAll();
                return $result;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
}