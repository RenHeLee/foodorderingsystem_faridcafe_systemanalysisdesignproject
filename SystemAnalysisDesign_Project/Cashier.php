<?php
include 'database.php';
include 'Customer.php';

class Cashier extends Customer
{
    /*
    * Get product type
    *
    * @param VOID
    * @return Mixed data type array
    */
    public function getProductType()
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT * FROM systemanalysisdesign.product_type");
            $query->execute();
            
            $result = $query->fetchAll();
            return $result;
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
    
    /*
     * Get food list
     *
     * @param VOID
     * @return Mixed data type array
     */
    public  function getFood()
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT * FROM systemanalysisdesign.food");
            $query->execute();
            
            $result = $query->fetchAll();
            return $result;
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
    
    /*
     * Get beverage list
     *
     * @param VOID
     * @return Mixed data type array
     */
    public function getBeverage()
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT * FROM systemanalysisdesign.beverage");
            $query->execute();
            
            $result = $query->fetchAll();
            return $result;
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
    
    /*
     * Get type buttons
     *
     * @param $btns
     * @return String
     */
    public function getTypesButton($btns)
    {
        $str = '';
        foreach ($btns as $key) {
            $str .= '<button type="button"  id="type" onClick="clickType(' . $key["product_type_id"] . ')" class ="btn btn-outline-primary" style="width: 300px; margin-left: 1%; margin-bottom: 1%">' . $key["type_name"] . '</button>';
        }
        return $str;
    }
    
    /*
     * Get items buttons
     *
     * @param $btns
     * @return String
     */
    public function getItemsButton($btns)
    {
        $str = '';
        foreach ($btns as $key) {
            $str .= '<button type="button" id="product" onClick="clickProduct(\'' . $key["product_id"] . '\', \'' . $key["product_name"] . '\', \'' . $key["price"] . '\')" class ="btn btn-outline-primary" style="width: 300px; margin-left: 1%; margin-bottom: 1%">' . $key["product_name"] . '(' . $key["price"] . ') </button>';
        }
        return $str;
    }
    
    /*
     * Insert data into temporary table
     *
     * @param $index, $product_id, $upSize, $quantity, $notes, $subtotalSingleItem, $productName
     * @return VOID
     */
    public function insertTempTable($index, $product_id, $upSize, $quantity, $notes, $subtotalSingleItem, $productName)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("INSERT INTO systemanalysisdesign.input VALUES (:index, :product_id, :up_size, :quantity, :notes, :subtotal, :product_name);");
            $query->bindParam(":index", $index, PDO::PARAM_STR);
            $query->bindParam(":product_id", $product_id, PDO::PARAM_STR);
            $query->bindParam(":up_size", $upSize, PDO::PARAM_STR);
            $query->bindParam(":notes", $notes, PDO::PARAM_STR);
            $query->bindParam(":quantity", $quantity, PDO::PARAM_STR);
            $query->bindParam(":subtotal", $subtotalSingleItem, PDO::PARAM_STR);
            $query->bindParam(":product_name", $productName, PDO::PARAM_STR);
            $query->execute();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
    
    /*
     * Drop temporary table
     *
     * @param VOID
     * @return VOID
     */
    public function dropTempTable()
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("DROP TABLE systemanalysisdesign.input");
            $query->execute();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
    
    /*
     * Create temporary table
     *
     * @param VOID
     * @return VOID
     */
    protected function createTempTable()
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("CREATE TABLE systemanalysisdesign.input( sold_product_id INT(10) NOT NULL PRIMARY KEY , product_id VARCHAR(5) NOT NULL, up_size INT(1) NOT NULL, quantity INT(3) NOT NULL, notes VARCHAR(500), subtotal NUMERIC(7,2) NOT NULL, product_name VARCHAR(100) NOT NULL )");
            $query->execute();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
    
    /*
     * Get food
     *
     * @param $index
     * @return VOID
     */
    public function deleteTempData($index)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("DELETE FROM systemanalysisdesign.input WHERE sold_product_id = :index");
            $query->bindParam(":index", $index, PDO::PARAM_STR);
            $query->execute();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
    
    /*
     * Get food
     *
     * @param $cashierid, $total, $discount, $grand, $extradisc, $serc, $befrounding, $roundingadj, $tendered, $transactiondate, $transactiontime
     * @return $receiptid
     */
    public function insertSalesRecordTable($cashierid, $total, $discount, $grand, $extradisc, $serc, $befrounding, $roundingadj, $tendered, $transactiondate, $transactiontime)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("INSERT INTO systemanalysisdesign.salesrecord
(cashierid, total, discount, grand, extradisc, serc, befrounding, roundingadj, tendered, transactiondate, transactiontime)
VALUES (:cashierid, :total, :discount, :grand, :extradisc, :serc, :befrounding, :roundingadj, :tendered, :transactiondate, :transactiontime);");
            $query->bindParam(":cashierid", $cashierid, PDO::PARAM_STR);
            $query->bindParam(":total", $total, PDO::PARAM_STR);
            $query->bindParam(":discount", $discount, PDO::PARAM_STR);
            $query->bindParam(":grand", $grand, PDO::PARAM_STR);
            $query->bindParam(":extradisc", $extradisc, PDO::PARAM_STR);
            $query->bindParam(":serc", $serc, PDO::PARAM_STR);
            $query->bindParam(":befrounding", $befrounding, PDO::PARAM_STR);
            $query->bindParam(":roundingadj", $roundingadj, PDO::PARAM_STR);
            $query->bindParam(":tendered", $tendered, PDO::PARAM_STR);
            $query->bindParam(":transactiondate", $transactiondate, PDO::PARAM_STR);
            $query->bindParam(":transactiontime", $transactiontime, PDO::PARAM_STR);
            $query->execute();
            $result = $db->lastInsertId();
            return $result;
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
    
    /*
     * Insert data from temporary table into sales table and reset temporary table. 
     *
     * @param $receiptid
     * @return VOID
     */
    public function insertSalesTable($receiptid)
    {
        $tempList = Cashier::getTempData();
        //echo $receiptid;
        try {
            global $pdo;
            $db = $pdo;
            $db->beginTransaction();
            if (Cashier::isMultidimensional($tempList)) {
                foreach ($tempList as $key) {
                    $product_id = $key["product_id"];
                    $up_size = $key['up_size'];
                    $notes = $key['notes'];
                    $quantity = $key['quantity'];
                    $product_status = 0;

                    $firstletter_id = substr($product_id, 0, 1);
                    if ($firstletter_id == "F") {
                        $product_type_id = 1;
                    } else {
                        $product_type_id = 2;
                    }
                    
                    $query = $db->prepare("INSERT INTO systemanalysisdesign.sales
(receiptid, product_id, up_size, product_type_id, notes, quantity, product_status)
VALUES (:receiptid, :product_id, :up_size, :product_type_id, :notes, :quantity, :product_status);");
                    $query->bindParam(":receiptid", $receiptid, PDO::PARAM_STR);
                    $query->bindParam(":product_id", $product_id, PDO::PARAM_STR);
                    $query->bindParam(":up_size", $up_size, PDO::PARAM_STR);
                    $query->bindParam(":product_type_id", $product_type_id, PDO::PARAM_STR);
                    $query->bindParam(":notes", $notes, PDO::PARAM_STR);
                    $query->bindParam(":quantity", $quantity, PDO::PARAM_STR);
                    $query->bindParam(":product_status", $product_status, PDO::PARAM_STR);
                    
                    $query->execute();
                }
            } else {
                $product_id = $tempList["product_id"];
                $up_size = $tempList['up_size'];
                $notes = $tempList['notes'];
                $quantity = $tempList['quantity'];
                $product_status = 0;
                
                $firstletter_id = substr($product_id, 0, 1);
                if ($firstletter_id == "F") {
                    $product_type_id = 1;
                } else {
                    $product_type_id = 2;
                }
                
                $query = $db->prepare("INSERT INTO systemanalysisdesign.sales
(receiptid, product_id, up_size, product_type_id, notes, quantity, product_status)
VALUES (:receiptid, :product_id, :up_size, :product_type_id, :notes, :quantity, :product_status);");
                $query->bindParam(":receiptid", $receiptid, PDO::PARAM_STR);
                $query->bindParam(":product_id", $product_id, PDO::PARAM_STR);
                $query->bindParam(":up_size", $up_size, PDO::PARAM_STR);
                $query->bindParam(":product_type_id", $product_type_id, PDO::PARAM_STR);
                $query->bindParam(":notes", $notes, PDO::PARAM_STR);
                $query->bindParam(":quantity", $quantity, PDO::PARAM_STR);
                $query->bindParam(":product_status", $product_status, PDO::PARAM_STR);
                
                $query->execute();
            }
            $db->commit();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
        Cashier::dropTempTable();
        Cashier::createTempTable();
    }
    
}