<?php

include 'database.php';

class Library
{
    /*
     * Check the array is multidimentional array
     *
     * @param $array
     * @return Boolean
     */
    public function isMultidimensional($array)
    {
        return count($array) !== count($array, COUNT_RECURSIVE);
    }
  
}