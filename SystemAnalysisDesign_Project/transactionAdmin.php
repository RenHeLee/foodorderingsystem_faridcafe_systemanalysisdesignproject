<?php
include 'Admin.php';

$app = new Admin();

$cashierid = $_GET['cashierid'];
$total = $_GET['total'];
$discount = $_GET['discount'];
$grand = $_GET['grand'];
$extradisc = $_GET['extradisc'];
$serc = $_GET['serc'];
$befrounding = $_GET['befrounding'];
$roundingadj = $_GET['roundingadj'];
$tendered = $_GET['tendered'];
$transactiondate = $_GET['date'];

date_default_timezone_set("Asia/Kuala_Lumpur");

$transactiontime = date("H:i:s");

$receiptid = $app->insertSalesRecordTable($cashierid, $total, $discount, $grand, $extradisc, $serc, $befrounding, $roundingadj, $tendered, $transactiondate, $transactiontime);

$app->insertBillSalesTable($receiptid);

echo"Done";