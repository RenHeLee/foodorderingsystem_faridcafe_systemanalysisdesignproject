<?php
include 'Cashier.php';

$app = new Cashier();

$cashierid = $_GET['cashierid'];
$total = $_GET['total'];
$discount = $_GET['discount'];
$grand = $_GET['grand'];
$extradisc = $_GET['extradisc'];
$serc = $_GET['serc'];
$befrounding = $_GET['befrounding'];
$roundingadj = $_GET['roundingadj'];
$tendered = $_GET['tendered'];

date_default_timezone_set("Asia/Kuala_Lumpur");

$transactiondate = date("Y-m-d");
$transactiontime = date("H:i:s");

$receiptid = $app->insertSalesRecordTable($cashierid, $total, $discount, $grand, $extradisc, $serc, $befrounding, $roundingadj, $tendered, $transactiondate, $transactiontime);

$app->insertSalesTable($receiptid);

echo"Done";