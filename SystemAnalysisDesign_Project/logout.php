<?php

// start session
session_start();

// Destroy user session
unset($_SESSION['user_id']);
unset($_SESSION['username']);
unset($_SESSION['user_post']);

// Redirect to index.php page
header("Location: index.php");
?>