<?php

try {
    $pdo = new PDO("mysql:host = '127.0.0.1:3306'; dbname = 'SystemAnalysisDesign'", 'root', '1234');
    // set the PDO error mode to exception
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "";
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}
?>