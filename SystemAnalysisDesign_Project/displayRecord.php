<?php
include 'Admin.php';
$app = new Admin();

$id = $_GET["id"];

$detail = $app->getSalesDetail($id);
echo "<div style=\"height: 8%;\" >";
echo "<h5>Sales detail of ID $id</h5>";
echo "</div>";

echo "<div style=\"overflow-y: scroll; height: 77%;\" >";
$app->displaySalesDetail($detail);
echo "</div>";

echo "<div style=\"height: 15%;\" >";
echo "<button type=\"button\" class=\"btn btn-primary\" onclick=\"voidBill(" . $id . ")\">Void";
echo "</div>";