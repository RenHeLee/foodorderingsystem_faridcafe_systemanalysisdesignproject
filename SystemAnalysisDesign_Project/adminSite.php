<?php

// Start Session
session_start();

// check user login
if ((empty($_SESSION['user_id'])) || ($_SESSION['user_id'] != 5)) {
    header("Location: index.php");
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<link rel="stylesheet" href="css/bootstrap-grid.css">
<link rel="stylesheet" href="css/bootstrap-grid.min.css">
<link rel="stylesheet" href="css/bootstrap-reboot.css">
<link rel="stylesheet" href="css/bootstrap-reboot.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<title>Admin</title>

<style>
#index {
	display: block;
}

#dailyReport {
	display: none;
}

#voidBill {
	display: none;
}

#insertBill {
	display: none;
}

font {
	color: #545454;
	font-weight: bolder;
}
</style>

</head>
<body>
	<script type="text/javascript">

    	function clickType(type) {
    	    var index = document.getElementById("index");
    	    var dailyReport = document.getElementById("dailyReport");
    	    var voidBill = document.getElementById("voidBill");
    	    
    		if (type == 1) {
    			index.style.display = "none";
    			dailyReport.style.display = "block";
    			voidBill.style.display = "none";
    		} else if (type == 2) {
    			index.style.display = "none";
    			dailyReport.style.display = "none";
    			voidBill.style.display = "block";
    			loadSalesReport();
    		} else if (type == 3) {
    			index.style.display = "block";
    			dailyReport.style.display = "none";
    			voidBill.style.display = "none";
    		}
    	}


		function loadDailyReport(){
			var date = document.getElementById("date").value;
			
			if (window.XMLHttpRequest) {
	            // code for IE7+, Firefox, Chrome, Opera, Safari
	            xmlhttp = new XMLHttpRequest();
	        } else {
	            // code for IE6, IE5
	            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	        }
	        xmlhttp.onreadystatechange = function() {
	            if (this.readyState == 4 && this.status == 200) {
	            	document.getElementById("displayDailyReport").innerHTML = this.responseText;
	            }
	        };
	        xmlhttp.open("GET","displayReport.php?date=" + date, true);
	        xmlhttp.send();        
	}

		function loadSalesReport(){
			var date = document.getElementById("date").value;
			
			if (window.XMLHttpRequest) {
	            // code for IE7+, Firefox, Chrome, Opera, Safari
	            xmlhttp = new XMLHttpRequest();
	        } else {
	            // code for IE6, IE5
	            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	        }
	        xmlhttp.onreadystatechange = function() {
	            if (this.readyState == 4 && this.status == 200) {
	            	document.getElementById("salesRecord").innerHTML = this.responseText;
	            }
	        };
	        xmlhttp.open("GET","displaySalesRecord.php", true);
	        xmlhttp.send();        
	}

		function displayRecord(id) {
			if (window.XMLHttpRequest) {
	            // code for IE7+, Firefox, Chrome, Opera, Safari
	            xmlhttp = new XMLHttpRequest();
	        } else {
	            // code for IE6, IE5
	            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	        }
	        xmlhttp.onreadystatechange = function() {
	            if (this.readyState == 4 && this.status == 200) {
	            	document.getElementById("salesDetail").innerHTML = this.responseText;
	            }
	        };
	        xmlhttp.open("GET","displayRecord.php?id=" + id, true);
	        xmlhttp.send();        
		}

		function voidBill(id) {
			if (confirm("Confirm to void this bill?")) {
			if (window.XMLHttpRequest) {
	            // code for IE7+, Firefox, Chrome, Opera, Safari
	            xmlhttp = new XMLHttpRequest();
	        } else {
	            // code for IE6, IE5
	            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	        }
	        xmlhttp.onreadystatechange = function() {
	            if (this.readyState == 4 && this.status == 200) {
	            	exitVoidBill();
	            }
	        };
	        xmlhttp.open("GET","voidBill.php?id=" + id, true);
	        xmlhttp.send(); 
			}   
		}

		function exitVoidBill(){
			clickType(3);
			document.getElementById("salesDetail").innerHTML = "";
		}
		
    	</script>
    	<?php
    include 'Admin.php';
    $report = new Admin();
    $firstDate = $report->getFirstDate();
    $lastDate = $report->getLastDate();
    ?>
		
		<div class="fluid-container"
		style="height: 100%; background-color: #1A1A1A; color: #545454; font-weight: bolder;">

		<div style="height: 10%">

			<nav class="navbar navbar-expand-sm bg-primary navbar-dark">
			<ul class="navbar-nav" style="color: #1A1A1A; font-weight: bolder;">
				<li class="nav-item"><h5
						style="text-align: center; font-weight: bolder;">Adminstrator of Farid Cafe</h5></li>
				<li class="nav-item"><a onclick="clickType(1)"
					style="margin-left: 50px;">Daily sales report</a></li>
				<li class="nav-item"><a onclick="clickType(2)"
					style="margin-left: 50px;">Void bill</a></li>
				<li class="nav-item"><a onclick="window.open('insertBill.php')"
					style="margin-left: 50px;">Insert bill</a></li>
			</ul>
			</nav>
			<table
				style="margin-left: 50px; color: #545454; font-weight: bolder;">
				<tr>
					<td>Cashier:</td>
					<td id="cashier">Farad</td>
				</tr>
			</table>
		</div>




		<div style="height: 85%; margin-left: 50px;">


			<div id="index">
				<button onclick="clickType(1)" class="btn btn-primary"
					style="width: 350px;">Daily sales report</button>
				<button onclick="clickType(2)" class="btn btn-primary"
					style="width: 350px;">Void bill</button>
				<button onclick="window.open('insertBill.php')"
					class="btn btn-primary" style="width: 350px;">Insert bill</button>
			</div>
			<div id="dailyReport">
				<div style="height: 5%"><h5>Daily report</h5></div>
				<div style="height: 15%">
					<label for="date">Choose a date for sales report</label>
			<?php echo "<input type=\"date\" id=\"date\" value=\"null\" min=\"" . $firstDate . "\" max=\"" . $lastDate . "\">"; ?>
			
			<button type="button" class="btn btn-primary" style="width: 100px"
						onclick="loadDailyReport()">Report</button>
					<button onclick="clickType(3)" class="btn btn-primary"
						style="width: 100px">Back</button>
				</div>
				<div id="displayDailyReport"
					style="height: 80%; overflow-y: scroll;"></div>
			</div>
			<div id="voidBill">
				<div style="height: 5%"><h5>Void bill</h5></div>
				<div id="salesRecord" style="height: 40%;"></div>
				<div id="salesDetail" style="overflow-y: scroll; height: 40%;"></div>
				<div style="overflow-y: height: 15%;">
					<button onclick="exitVoidBill()" class="btn btn-outline-primary"
						style="width: 350px">Back</button>
				</div>
			</div>
		</div>



		<div style="height: 5%">
			<p>
				Copyright (c) 2018 BIC21003 System Analysis Design <a
					href="logout.php">Logout</a>
			</p>
		</div>

	</div>




</body>

</html>